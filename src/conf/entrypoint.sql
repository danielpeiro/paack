CREATE TABLE "customers" (
  "id" integer NOT NULL,
  "status" character varying(50) NOT NULL,
  "created_at" time NOT NULL,
  "updated_at" time NOT NULL,
  "name" character varying(200) NOT NULL,
  "email" character varying(200) NOT NULL,
  "phone" character varying(30) NOT NULL
);

CREATE UNIQUE INDEX primary_key ON "customers" ("id");
