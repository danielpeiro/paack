package main

import (
        "database/sql"
        "fmt"
        "log"
        "strings"
        "time"
)

// repository is a customer repository
type repository interface {
        buildBulkInsert(customers []*customer) error
}

// newRepository returns the default repository (postgres)
func newRepository(db *sql.DB) repository {
        return &postgresRepository{
                db: db,
        }
}

// postgresRepository is the customer repository for postgres
type postgresRepository struct {
        db *sql.DB
}

// buildBulkInsert massive insert or update customer's data
func (r *postgresRepository) buildBulkInsert(customers []*customer) error {
        val := []interface{}{}
        for _, c := range customers {
                val = append(val, c.ID, c.Status, c.CreatedAt, time.Now(), c.Name, c.Email, c.Phone)
        }

        key := []string{}
        for i := range customers {
                k := []string{}
                for ii := 1; ii < 8; ii++ {
                        k = append(k, fmt.Sprintf("$%d", i*7+ii))
                }

                key = append(key, fmt.Sprintf("(%s)", strings.Join(k, ",")))
        }

        keys := strings.Join(key, ", ")
        sql := fmt.Sprintf("INSERT INTO customers (id, status, created_at, updated_at, name, email, phone) VALUES %s ", keys)
        sql = sql + "ON CONFLICT (id) DO UPDATE "
        sql = sql + "SET updated_at = EXCLUDED.updated_at, name = EXCLUDED.name, email = EXCLUDED.email, phone = EXCLUDED.phone;"
        stmt, err := r.db.Prepare(sql)
        if err != nil {
                return err
        }
        defer stmt.Close()

        _, err = stmt.Exec(val...)
        if err != nil {
                return err
        }

        log.Println("database", customers[0].Name)

        return nil
}

