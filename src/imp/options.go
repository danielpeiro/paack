package main

import (
	"os"
	"strconv"
)

const (
	defaultMaxJobs          uint8 = 10
	defaultCSVFilename            = "csv"
	defaultDatabaseHost           = "localhost"
	defaultDatabasePort           = "5432"
	defaultDatabaseName           = "postgres"
	defaultDatabaseUser           = "postgres"
	defaultDatabasePassword       = "123456"
)

func optionsApi() *apiOptions {
	return &apiOptions{
		csvFilename: envOrDefaultStringValue("CSV_FILENAME", defaultCSVFilename),
		maxJobs:     uint8(envOrDefaultIntValue("MAX_JOBS", int(defaultMaxJobs))),
	}
}

func optionsDatabase() *databaseOptions {
	return &databaseOptions{
		Host:         envOrDefaultStringValue("DB_HOST", defaultDatabaseHost),
		Port:         envOrDefaultStringValue("DB_PORT", defaultDatabasePort),
		DatabaseName: envOrDefaultStringValue("DB_NAME", defaultDatabaseName),
		User:         envOrDefaultStringValue("DB_USER", defaultDatabaseUser),
		Password:     envOrDefaultStringValue("DB_PASSWORD", defaultDatabasePassword),
	}
}

func envOrDefaultStringValue(key, dft string) string {
	if os.Getenv(key) != "" {
		return os.Getenv(key)
	}

	return dft
}

func envOrDefaultIntValue(key string, dft int) int {
        if os.Getenv(key) != "" {
                str := os.Getenv(key)
                v, err := strconv.Atoi(str)
                if err != nil {
                        return dft
                }

                return v
        }

        return dft
}
