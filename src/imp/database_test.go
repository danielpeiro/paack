package main

import (
	"strings"
	"testing"
)

func Test__OK(t *testing.T) {
	opts := databaseOptions{
		Host:         defaultDatabaseHost,
		Port:         defaultDatabasePort,
		DatabaseName: defaultDatabaseName,
		User:         defaultDatabaseUser,
		Password:     defaultDatabasePassword,
	}

	db := newDatabaseManager(&opts)
	err := db.open()
	if err != nil {
		t.Fatalf("Error opening database connection: %v", err)
	}

	err = db.ping()
	if err != nil {
		t.Errorf("Error pinging database: %v", err)
	}

	err = db.close()
	if err != nil {
		t.Errorf("Error closing database connection: %v", err)
	}
}

func Test__InvalidCredentials(t *testing.T) {
	opts := databaseOptions{
		Host:         defaultDatabaseHost,
		Port:         defaultDatabasePort,
		DatabaseName: defaultDatabaseName,
		User:         "test",
		Password:     "test",
	}

	db := newDatabaseManager(&opts)
	err := db.open()
	if err != nil {
		t.Fatalf("Error opening database connection: %v", err)
	}

	err = db.ping()
	if err == nil {
		t.Errorf("send() error = we expect an error")
	}

	if !strings.Contains(err.Error(), "authentication failed") {
		t.Errorf("send() error = authentication must be fail")
	}

	err = db.close()
	if err != nil {
		t.Errorf("Error closing database connection: %v", err)
	}
}
