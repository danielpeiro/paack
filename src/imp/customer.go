package main

import (
        "time"
)

// customer is the domain model
type customer struct {
        ID    int               `json:"id"`
        Status string           `jason:"status"`
        CreatedAt time.Time     `json:"created_at"`
        UpdatedAt time.Time     `json:"updated_at"`
        Name  string            `json:"name"`
        Email string            `json:"email"`
        Phone string            `json:"phone"`
}
