package main

import (
        "errors"
        "log"
        "net/http"
)

const (
        uploadPath = "/upload"
)

var (
        invalidMaxJobs = errors.New("invalid MAX_JOBS value")
)

func main() {
        mng := newDatabaseManager(optionsDatabase())
        db, err := mng.open()
        if err != nil {
                log.Fatal(err)
        }

        repo := newRepository(db)
        api := newApi(optionsApi(), repo)

        mux := http.NewServeMux()
        mux.HandleFunc(uploadPath, api.uploadHandler)
        http.ListenAndServe(":5000", mux)
}
