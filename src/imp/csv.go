package main

import (
        "encoding/csv"
        "io"
        "os"
        "strconv"
        "time"
)

const (
        groupSize = 1000
        groupBuffer = 10
)
type customerGroupResult struct {
        err error
        customers []*customer
}

func readCustomerGroup(filepath string, result chan customerGroupResult) {
        f, err := os.Open(filepath)
        if err != nil {
                result <- customerGroupResult{ err: err }
        }

        customers := make([]*customer, 0, groupSize)
        rd := csv.NewReader(f)
        for {
                data, err := rd.Read()
                if err == io.EOF {
                        break
                }

                if err != nil {
                        result <- customerGroupResult{ err: err }
                        return
                }

                c, err := parseCustomer(data)
                if err != nil {
                        result <- customerGroupResult{ err: err }
                        return
                }

                customers = append(customers, c)
                if len(customers) == groupSize {
                        result <- customerGroupResult{ customers: customers }
                        customers = make([]*customer, 0, groupSize)
                }
        }

        if len(customers) > 0 {
                result <- customerGroupResult{ customers: customers }
        }
}

func parseCustomer(data []string) (*customer, error) {
        id, err := strconv.Atoi(data[0])
        if err != nil {
                return nil, err
        }

        c := customer{
                ID:    id,
                CreatedAt: time.Now(),
                Name:  data[1],
                Email: data[2],
                Phone: data[3],
        }

        return &c, nil
}
