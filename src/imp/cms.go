package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const defaultCmsApiCustomerPath = "/customer"

var (
	invalidCustomer = errors.New("invalid customer data")
	invalidMethod   = errors.New("method not allowed")
	invalidURL      = errors.New("invalid URL")
)

// CmsApi interact with CMS service
type CmsApi struct {
	url    string
	client *http.Client
}

// NewCmsApi returns new CmsApi instance
func NewCmsApi(host string) *CmsApi {
	return &CmsApi{
		url:    host + defaultCmsApiCustomerPath,
		client: &http.Client{},
	}
}

// Add sends a new customer data
func (api *CmsApi) Add(c *customer) error {
	if c == nil {
		return invalidCustomer
	}

	return api.send("add", c)
}

// Update updates data od a customer
func (api *CmsApi) Update(c *customer) error {
	if c == nil {
		return invalidCustomer
	}

	return api.send("update", c)
}

func (api *CmsApi) send(action string, c *customer) error {
	data, err := json.Marshal(c)
	if err != nil {
		return err
	}

	method := http.MethodPost
	switch action {
	case "add":
		method = http.MethodPost
		break
	case "update":
		method = http.MethodPut
		break
	default:
		return fmt.Errorf("action not allowed: %s", action)
	}

	req, err := http.NewRequest(method, api.url, strings.NewReader(string(data)))
	if err != nil {
		return err
	}

	cli := &http.Client{}

	resp, err := cli.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		msj := strings.TrimSuffix(string(data), "\n")
		return fmt.Errorf(msj)
	}

	return nil
}
