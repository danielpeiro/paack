package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"sync"
	"time"

	"github.com/google/uuid"
)

const (
	multipartMaxMemorySize = 1 << 30
)

var (
	maxJobsExceeded = errors.New("exceeded limit of active jobs")
)

type api struct {
	sync.Mutex
	count   uint8
	jobs    chan job
	pool    chan job
	repo    repository
	options *apiOptions
}

type apiOptions struct {
	maxJobs     uint8
	csvFilename string
}

// newApi initialize and return a new api instance
func newApi(opts *apiOptions, repo repository) *api {
	api := &api{
		repo:    repo,
		options: opts,
	}

	api.preparePool()

	return api
}

// preparePool is a ordered pool than execute only one job per cycle
func (api *api) preparePool() {
	api.jobs = make(chan job)
	api.pool = make(chan job, api.options.maxJobs)

	go func() {
		for j := range api.jobs {
			err := api.execute(j)
			if err != nil {
				log.Println(err)
			}
		}
	}()

	go func() {
		for j := range api.pool {
			api.jobs <- j
		}
	}()
}

func (api *api) execute(j job) error {
        group := make(chan customerGroupResult, groupBuffer)
        defer close(group)

        wg := &sync.WaitGroup{}
        wg.Add(1)
	go func() {
                for g := range group {
                        wg.Add(1)
                        go func(g customerGroupResult) {
                                log.Println("api", g.customers[0].Name)

                                if g.err != nil {
                                        log.Fatal(g.err)
                                        // return g.err
                                }

                                err := api.repo.buildBulkInsert(g.customers)
                                if err != nil {
                                        log.Fatal(err)
                                        // return err
                                }

                                time.Sleep(time.Second)
                                wg.Done()
                        }(g)
                }

                wg.Done()
        }()

        readCustomerGroup(j.filepath, group)

        wg.Wait()

        api.Lock()
	api.count--
	api.Unlock()

	return nil
}

func (api *api) uploadHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPut:
		api.Mutex.Lock()

		if api.count == api.options.maxJobs {
			api.Mutex.Unlock()
			http.Error(w, maxJobsExceeded.Error(), http.StatusInternalServerError)
			return
		}

		api.count++
		api.Mutex.Unlock()

		filepath, err := api.download(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		j := job{
			startedAt: time.Now(),
			filepath:  filepath,
		}

		api.pool <- j
	default:
		http.Error(w, "Invalid method.", http.StatusMethodNotAllowed)
	}
}

func (api *api) download(r *http.Request) (string, error) {
	r.ParseMultipartForm(multipartMaxMemorySize)
	src, _, err := r.FormFile(api.options.csvFilename)
	if err != nil {
		return "", err
	}
	defer src.Close()

	UUID := uuid.New()
	filename := fmt.Sprintf("%s.csv", UUID.String())
	filepath := path.Join("/tmp", filename)
	dst, err := os.Create(filepath)
	if err != nil {
		return "", err
	}
	defer dst.Close()

	_, err = io.Copy(dst, src)
	if err != nil {
		return "", err
	}

	return filepath, nil
}
