package main

import (
        "time"
)

type job struct {
        startedAt	time.Time
        finishedAt	time.Time
        filepath	string
}

