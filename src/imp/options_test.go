package main

import (
	"os"
	"strconv"
	"testing"
)

func Test__optionsApi_default__OK(t *testing.T) {
	opts, err := optionsApi()
	if err != nil {
		t.Error("optionsApi() = invalid default options")
	}

	if opts.maxJobs != defaultMaxJobs {
		t.Errorf("optionsApi() = invalid default max jobs: expected: %d, found: %d", defaultMaxJobs, opts.maxJobs)
	}

	if opts.csvFilename != defaultCSVFilename {
		t.Errorf("optionsApi() = invalid default cvs filename: expected: %s, found: %s", defaultCSVFilename, opts.csvFilename)
	}
}

func Test__optionsApi__OK(t *testing.T) {
	expected := &apiOptions{
		maxJobs:     5,
		csvFilename: "test",
	}

	os.Setenv("MAX_JOBS", strconv.Itoa(int(expected.maxJobs)))
	os.Setenv("CSV_FILENAME", expected.csvFilename)

	opts, err := optionsApi()
	if err != nil {
		t.Error("optionsApi() = invalid options")
	}

	if opts.maxJobs != expected.maxJobs {
		t.Errorf("optionsApi() = invalid max jobs: expected: %d, found: %d", expected.maxJobs, opts.maxJobs)
	}

	if opts.csvFilename != expected.csvFilename {
		t.Errorf("optionsApi() = invalid cvs filename: expected: %s, found: %s", expected.csvFilename, opts.csvFilename)
	}
}
