package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"
)

func server(method string) *httptest.Server {
	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != method {
			http.Error(w, invalidMethod.Error(), http.StatusMethodNotAllowed)
			return
		}

		if r.URL.Path != defaultCmsApiCustomerPath {
			http.Error(w, invalidURL.Error(), http.StatusNotFound)
			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, invalidCustomer.Error(), http.StatusBadRequest)
			return
		}

		var c customer
		err = json.Unmarshal(body, &c)
		if err != nil {
			http.Error(w, "invalid customer data", http.StatusBadRequest)
			return
		}

		if c.ID == 0 {
			http.Error(w, "invalid customer data", http.StatusBadRequest)
			return
		}
	}))

	return s
}

func customerMock() *customer {
	return &customer{
		ID:        1,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Name:      "Paack SPV Investments, SL.",
		Email:     "info@paak.co",
	}
}

func Test__Add__OK(t *testing.T) {
	s := server(http.MethodPost)
	defer func() { s.Close() }()

	api := NewCmsApi(s.URL)
	api.client = s.Client()

	err := api.Add(customerMock())

	if err != nil {
		t.Errorf("addCustomer() error = %v", err)
	}
}

func Test__Add__InvalidCustomer(t *testing.T) {
	s := server(http.MethodPost)
	defer func() { s.Close() }()

	api := NewCmsApi(s.URL)
	api.client = s.Client()

	err := api.Add(nil)
	if err == nil {
		t.Errorf("send() error = we expect an error")
	}

	if err.Error() != invalidCustomer.Error() {
		t.Errorf("addCustomer() error = %v", err)
	}
}

func Test__Add__InvalidMethod(t *testing.T) {
	s := server(http.MethodPut)
	defer func() { s.Close() }()

	api := NewCmsApi(s.URL)
	api.client = s.Client()

	err := api.Add(customerMock())
	if err == nil {
		t.Errorf("send() error = we expect an error")
	}

	if err.Error() != invalidMethod.Error() {
		t.Errorf("addCustomer() error = %v", err)
	}
}

func Test__Add__InvalidURL(t *testing.T) {
	s := server(http.MethodPost)
	defer func() { s.Close() }()

	api := NewCmsApi(s.URL)
	api.url += "/test"
	api.client = s.Client()

	err := api.Add(customerMock())
	if err == nil {
		t.Errorf("send() error = we expect an error")
	}

	if err.Error() != invalidURL.Error() {
		t.Errorf("addCustomer() error = %v", err)
	}
}

func Test__Update__OK(t *testing.T) {
	s := server(http.MethodPut)
	defer func() { s.Close() }()

	api := NewCmsApi(s.URL)
	api.client = s.Client()

	err := api.Update(customerMock())

	if err != nil {
		t.Errorf("addCustomer() error = %v", err)
	}
}

func Test__Update__InvalidCustomer(t *testing.T) {
	s := server(http.MethodPut)
	defer func() { s.Close() }()

	api := NewCmsApi(s.URL)
	api.client = s.Client()

	err := api.Update(nil)
	if err == nil {
		t.Errorf("send() error = we expect an error")
	}

	if err.Error() != invalidCustomer.Error() {
		t.Errorf("addCustomer() error = %v", err)
	}
}

func Test__Update__InvalidMethod(t *testing.T) {
	s := server(http.MethodPost)
	defer func() { s.Close() }()

	api := NewCmsApi(s.URL)
	api.client = s.Client()

	err := api.Update(customerMock())
	if err == nil {
		t.Errorf("send() error = we expect an error")
	}

	if err.Error() != invalidMethod.Error() {
		t.Errorf("addCustomer() error = %v", err)
	}
}

func Test__Update__InvalidURL(t *testing.T) {
	s := server(http.MethodPut)
	defer func() { s.Close() }()

	api := NewCmsApi(s.URL)
	api.url += "/test"
	api.client = s.Client()

	err := api.Update(customerMock())
	if err == nil {
		t.Errorf("send() error = we expect an error")
	}

	if err.Error() != invalidURL.Error() {
		t.Errorf("addCustomer() error = %v", err)
	}
}

// func Test_sendCustomer__add_ok(t *testing.T) {
// 	s := server(http.MethodPost)
// 	defer func() { s.close() }()
//
// 	err := send("add", customerMock())
// 	if err != nil {
// 		t.Errorf("send() error = %v", err)
// 	}
// }
//
// func Test_sendCustomer__update_ok(t *testing.T) {
// 	s := server(http.MethodPut)
// 	defer func() { s.close() }()
//
// 	err := send("update", customerMock())
// 	if err != nil {
// 		t.Errorf("send() error = %v", err)
// 	}
// }
//
// func Test_sendCustomer__action_not_allowed(t *testing.T) {
// 	s := server(http.MethodPost)
// 	defer func() { s.close() }()
//
// 	expected := "action not allowed: test"
//
// 	err := send("test", customerMock())
// 	if err == nil {
// 		t.Errorf("send() error = we expect an error")
// 	}
//
// 	if err.Error() !=  expected {
// 		t.Errorf("send() error = %v", err)
// 	}
// }
//
// func Test_sendCustomer__invalid_customer(t *testing.T) {
// 	s := server(http.MethodPost)
// 	defer func() { s.close() }()
//
// 	expected := "CMS status response: 400 Bad Request"
//
// 	err := send("add", nil)
// 	if err == nil {
// 		t.Errorf("send() error = we expect an error")
// 	}
//
// 	if err.Error() != expected {
// 		t.Errorf("send() error = %v", err)
// 	}
// }
//
// func Test_addCustomer__ok(t *testing.T) {
// 	s := server(http.MethodPost)
// 	defer func() { s.close() }()
//
// 	err := addCustomer(customerMock())
// 	if err != nil {
// 		t.Errorf("addCustomer() error = %v", err)
// 	}
// }
//

func TestNewCmsApi(t *testing.T) {
	type args struct {
		host string
	}
	tests := []struct {
		name string
		args args
		want *CmsApi
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewCmsApi(tt.args.host); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCmsApi() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCmsApi_Add(t *testing.T) {
	type args struct {
		c *customer
	}
	tests := []struct {
		name    string
		api     *CmsApi
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.api.Add(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("CmsApi.Add() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCmsApi_Update(t *testing.T) {
	type args struct {
		c *customer
	}
	tests := []struct {
		name    string
		api     *CmsApi
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.api.Update(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("CmsApi.Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCmsApi_send(t *testing.T) {
	type args struct {
		action string
		c      *customer
	}
	tests := []struct {
		name    string
		api     *CmsApi
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.api.send(tt.args.action, tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("CmsApi.send() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
