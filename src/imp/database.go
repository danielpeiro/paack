package main

import (
        "database/sql"
        "errors"
        "fmt"

        _ "github.com/lib/pq" // postgresManager driver
)

var (
        unableToConnect = errors.New("unable to connect to database")
)

// database defines the interface for all database managers
type database interface {
        open() (*sql.DB, error)
        close() error
        ping() error
}

// newDatabaseManager returns the default database manager (postgres)
func newDatabaseManager(opts *databaseOptions) database {
        return newPostgresManager(opts)
}

// databaseOptions defines the database configuration options
type databaseOptions struct {
        Host         string
        Port         string
        DatabaseName string
        User         string
        Password     string
}

// postgresManager is the database manager for postgresManager
type postgresManager struct {
        options *databaseOptions
        db      *sql.DB
}

// newPostgresManager return new postgresManager manager instance
func newPostgresManager(opts *databaseOptions) database {
        return &postgresManager{
                options: opts,
        }
}

// open open the connection
func (p *postgresManager) open() (*sql.DB, error) {
        connStr := p.connectionString()
        for i := 0; i < 10; i++ {
                db, err := sql.Open("postgres", connStr)
                if err == nil {
                        db.SetMaxOpenConns(98)
                        p.db = db
                        return db, nil
                }
        }

        return 	nil, unableToConnect
}

func (p *postgresManager) connectionString() string {
        return fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
                p.options.Host,
                p.options.Port,
                p.options.DatabaseName,
                p.options.User,
                p.options.Password,
        )
}

// close close the connection
func (p *postgresManager) close() error {
        return p.db.Close()
}

// ping send a ping to database server
func (p *postgresManager) ping() error {

        return p.db.Ping()
}
