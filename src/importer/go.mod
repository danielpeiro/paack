module gitlab.com/danielpeiro/paack/importer

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.1.1
)
