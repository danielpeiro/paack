package importer

import (
        "encoding/csv"
        "net/http"
)

// CSVSourceManager define the interface for all csv source managers
type CSVSourceManager interface {
        Reader() (*csv.Reader, error)
}

// NewCSVSourceManager returns the default source manager (downloader)
func NewCSVSourceManager(csv string, request *http.Request) CSVSourceManager {
        return &DownloaderSourceManager{
                filename: csv,
                request:  request,
        }
}

// DownloaderSourceManager downloads csv file
type DownloaderSourceManager struct {
        filename string
        request  *http.Request
}

func (s *DownloaderSourceManager) Reader() (*csv.Reader, error) {
        err := s.request.ParseMultipartForm(multipartMaxMemorySize)
        if err != nil {
                return nil, err
        }

        f, _, err := s.request.FormFile(s.filename)
        if err != nil {
                return nil, err
        }

        return csv.NewReader(f), nil
}
