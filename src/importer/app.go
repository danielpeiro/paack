package importer

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"sync"
	"time"

	"github.com/google/uuid"
)

const multipartMaxMemorySize = 1 << 30

type job struct {
	startedAt	time.Time
	finishedAt	time.Time
	filepath	string
}
// AppOptions defines application configuration options
type AppOptions struct {
	MaxJobs		uint8
	CSVFilename     string
	DatabaseOptions *DatabaseOptions
}

// App defines the application
type App struct {
	sync.Mutex
	count 	uint8
	db      Database
	repo    Repository
	source  CSVSourceManager
	dispatcher chan job
	options *AppOptions
}

// NewApp returns a new instance of the application
func NewApp(opts *AppOptions) *App {
	app := App{
		options: opts,
	}

	return &app
}

// Do runs the application
func (app *App) Do() error {
	jobs := make(chan job)
	go func() {
		for i := range jobs {
			log.Println(i)
		}
	}()

	app.dispatcher = make(chan job, app.options.MaxJobs)
	go func() {
		for i := range app.dispatcher {
			jobs <- i
		}
	}()

	app.db = NewDatabase(app.options.DatabaseOptions)
	err := app.db.Open()
	if err != nil {
		log.Fatal(err)
	}

	app.repo = NewRepository(app.db)

	mux := http.NewServeMux()
	mux.HandleFunc("/api/upload", app.UploadHandler)
	return http.ListenAndServe(":5000", mux)
}

// UploadHandler import customer filename file
func (app *App) UploadHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPut:
		app.Mutex.Lock()

		if app.count == app.options.MaxJobs  {
			app.Mutex.Unlock()
			http.Error(w, "Exceeded limit of active jobs", http.StatusInternalServerError)
			return
		}

		app.count++
		app.Mutex.Unlock()

		filepath, err := app.download(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		j := job{
			startedAt:	time.Now(),
			filepath:	filepath,
		}
		app.dispatcher <- j
	default:
		http.Error(w, "Invalid method.", http.StatusMethodNotAllowed)
	}
}

func (app *App) download(r *http.Request) (string, error) {
	r.ParseMultipartForm(multipartMaxMemorySize)
	src, _, err := r.FormFile(app.options.CSVFilename)
	if err != nil {
		return "", err
	}
	defer src.Close()

	UUID := uuid.New()
	filename := fmt.Sprintf("%s.csv", UUID.String())
	filepath := path.Join("/tmp", filename)
	dst, err := os.Create(filepath)
	if err != nil {
		return "", err
	}
	defer dst.Close()

	_, err = io.Copy(dst, src)
	if err != nil {
		return "", err
	}

	return filepath, nil
}

func (app *App) parse(r *http.Request) (string, error) {
	UUID := uuid.New()
	filename := fmt.Sprintf("%s.json", UUID.String())
	filepath := path.Join("/tmp", filename)
	dst, err := os.Create(filepath)
	if err != nil {
		return "", err
	}
	defer dst.Close()

	_, err = dst.Write([]byte("["))
	if err != nil {
		return "", err
	}

	r.ParseMultipartForm(multipartMaxMemorySize)
	src, _, err := r.FormFile(app.options.CSVFilename)
	if err != nil {
		return "", err
	}
	defer src.Close()

	rd := csv.NewReader(src)

	for {
		data, err := rd.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			return "", err
		}

		c, err := parseCustomer(data)
		if err != nil {
			return "", err
		}

		j, err := json.Marshal(&c)
		if err != nil {
			return "", err
		}

		_, err = dst.WriteString(string(j))
		if err != nil {
			return "", err
		}
	}

	_, err = dst.Write([]byte("]"))
	if err != nil {
		return "", err
	}

	return filepath, nil
}

func parseCustomer(data []string) (*Customer, error) {
	id, err := strconv.Atoi(data[0])
	if err != nil {
		return nil, err
	}

	c := Customer{
		ID:    id,
		Name:  data[1],
		Phone: data[2],
	}

	return &c, nil
}

// Dispose close the database connection
func (app *App) Dispose() error {
	if app.db != nil {
		return app.db.Close()
	}

	return nil
}
