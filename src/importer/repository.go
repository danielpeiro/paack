package importer

// Repository is a customer repository
type Repository interface {
	BuildBulkInsert(customers []*Customer)
}

// NewRepository returns the default repository (postgres)
func NewRepository(db Database) Repository {
	return NewPostgresRepository(db)
}

// PostgresRepository is the customer repository for postgres
type PostgresRepository struct {
	db Database
}

// NewPostgresRepository returns a new CustomerRepository instance
func NewPostgresRepository(db Database) Repository {
	return &PostgresRepository{
	        db: db,
        }
}

// buildBulkInsert massive insert or update customer's data
func (r *PostgresRepository) BuildBulkInsert(customers []*Customer) {}
