package importer

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq" // Postgres driver
)

// Database defines the interface for all database managers
type Database interface {
	Open() error
	Close() error
}

// NewDatabase returns the default database manager (postgres)
func NewDatabase(opts *DatabaseOptions) Database {
	return NewPostgres(opts)
}

// DatabaseOptions defines the database configuration options
type DatabaseOptions struct {
	Host         string
	Port         string
	DatabaseName string
	User         string
	Password     string
}

// Postgres defines the Postgres database manager
type Postgres struct {
	options *DatabaseOptions
	db      *sql.DB
}

// NewPostgres return new Postgres manager instance
func NewPostgres(opts *DatabaseOptions) Database {
	return &Postgres{
		options: opts,
	}
}

// open open the connection
func (p *Postgres) Open() error {
	log.Println("Connecting to postgres...")

	connStr := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
		p.options.Host,
		p.options.Port,
		p.options.DatabaseName,
		p.options.User,
		p.options.Password,
	)

	for i := 0; i < 10; i++ {
		db, err := sql.Open("postgres", connStr)
		if err == nil {
			p.db = db
			log.Println("Connected to postgres")
			return nil
		}
	}

	return 	fmt.Errorf("unable to connect to postgres")
}

// close close the connection
func (p *Postgres) Close() error {
	return p.db.Close()
}
