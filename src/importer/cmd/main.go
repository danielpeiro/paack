package main

import (
	"log"
	"os"
	"strconv"

	"gitlab.com/danielpeiro/paack/importer"
)

const defaultCSVFilename = "csv"

func main() {
	app := importer.NewApp(configure())
	defer app.Dispose()
	log.Fatal(app.Do())
}

func configure() *importer.AppOptions {
	required := []string{"DB_HOST", "DB_PORT", "DB_NAME", "DB_USER", "DB_PASSWORD", "CSV_FILENAME", "MAX_JOBS"}
	for _, o := range required {
		if os.Getenv(o) == "" {
			log.Fatalf("%s is required", o)
		}
	}

	dbOpts := importer.DatabaseOptions{
                Host:         os.Getenv("DB_HOST"),
		Port:         os.Getenv("DB_PORT"),
		DatabaseName: os.Getenv("DB_NAME"),
		User:         os.Getenv("DB_USER"),
		Password:     os.Getenv("DB_PASSWORD"),
	}

	maxJobs, err := strconv.ParseUint(os.Getenv("MAX_JOBS"), 10, 8)
	if err != nil {
		log.Fatal("MAX_JOBS value is not valid")
	}

	appOpts := importer.AppOptions{
                CSVFilename:            os.Getenv("CSV_FILENAME"),
		MaxJobs: 		uint8(maxJobs),
                DatabaseOptions:	&dbOpts,
        }

	return &appOpts
}
