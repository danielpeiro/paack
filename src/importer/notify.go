package importer

import (
        "fmt"
)

func notify(ch chan<- string) {
        defer close(ch)
        for str := range ch {
                fmt.Println("Notify", str)
        }
}