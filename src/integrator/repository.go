package integrator

// Repository is a customer repository
type Repository interface {
	BuildBulkInsert(customers []*Customer)
}

// PostgresRepository is the customer repository for postgres
type PostgresRepository struct {
	db Database
}

// NewPostgresRepository returns a new CustomerRepository instance
func NewPostgresRepository(db Database) Repository {
	return &PostgresRepository{}
}

// buildBulkInsert massive insert or update customer's data
func (r *PostgresRepository) BuildBulkInsert(customers []*Customer) {}
