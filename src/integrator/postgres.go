package integrator

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq" // Postgres driver
)

const (
	defaultHost   = "db"
	defaultPort   = "5432"
	defaultDBName = "customers"
)

// Database is a database manager interface
type Database interface {
	Open()
	Close() error
}

// Postgres is the PostgresSQL manager
type Postgres struct {
	user     string
	password string
	DB       *sql.DB
}

// NewPostgres return new Postgres manager instance
func NewPostgres(user string, password string) Database {
	return &Postgres{
		user:     user,
		password: password,
	}
}

func (m *Postgres) connectionString() string {
	host := os.Getenv("POSTGRESS_HOST")
	if host == "" {
		host = defaultHost
	}

	port := os.Getenv("POSTGRESS_PORT")
	if port == "" {
		port = defaultPort
	}

	dbname := os.Getenv("POSTGRES_DBNAME")
	if dbname == "" {
		dbname = defaultDBName
	}

	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host,
		port,
		m.user,
		m.password,
		dbname)

	return connStr
}

// open open the connection
func (m *Postgres) Open() {
	log.Println("Connecting to postgres...")

	for i := 0; i < 10; i++ {
		db, err := sql.Open("postgres", m.connectionString())
		if err == nil {
			m.DB = db
			log.Println("Connected")
			return
		}
	}

	log.Fatal("Unable to connect to postgres")
}

// close close the connection
func (m *Postgres) Close() error {
	return m.DB.Close()
}
