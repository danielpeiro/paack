package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/danielpeiro/paack/integrator"
)

func main() {
	db := integrator.NewPostgres(databaseCredentials())
	defer db.Close()

	repo := integrator.NewPostgresRepository(db)

	app := integrator.NewApp(repo)

	mux := http.NewServeMux()
	mux.HandleFunc("/api/customer", app.CustomerHandler)
	http.ListenAndServe(":5001", mux)
}

func databaseCredentials() (string, string) {
	user := os.Getenv("DB_USER")
	if user == "" {
		log.Fatal("Invalid DB_USER")
	}

	password := os.Getenv("DB_PASSWORD")
	if password == "" {
		log.Fatal("Invalid DB_PASSWORD")
	}

	return user, password
}
