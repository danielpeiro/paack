package integrator

import (
	"net/http"
)

// App ins the main app
type App struct {
	repo Repository
}

// NewApp returns a new API instance
func NewApp(repo Repository) *App {
	return &App{
		repo: repo,
	}
}

// CustomerHandler add or update customer data
func (app *App) CustomerHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		w.Write([]byte("integrator: añadimos cliente"))
	case http.MethodPut:
		w.Write([]byte("integrator: actualizamos cliente"))
	default:
		http.Error(w, "Invalid method.", http.StatusMethodNotAllowed)
	}
}
